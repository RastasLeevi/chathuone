  // Defines all required connections
  var app = require('express')();
  var http = require('http').Server(app);
  var io = require('socket.io')(http);

  // Loads the defined .html file
  app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
  });
  // Array for tracking previously input names
  users = [];
  // Upon connection sends a message to console and checks for cookie existence + sets username
  io.on('connection', function(socket){
    console.log('an user connected');
    console.log(users);
    socket.on('setUsername', function(data) {
      console.log(data);

      if(!data.cookie && users.indexOf(data.name) > -1) {
        socket.emit('userExists', data.name + ' Username is already taken.');
      } else {
        users.push(data.name);
        socket.emit('userSet', {username: data.name});
        console.log(users);
      }
  });
    // Displays a new message
    socket.on('msg', function(data) {
      io.sockets.emit('newmsg', data);
    });
    // Upon user disconnection sends a message to the console
    socket.on('disconnect', function(){
      console.log('user disconnected');
    });
  });

  // Listens to the defined port, 8080 allows other people to connect, '0.0.0.0' allows connection
  http.listen(8080, '0.0.0.0', function(){
    console.log('listening on *:8080');
  });
